# [SoftUni](https://softuni.bg) [SOFTWARE ENGINEERING](https://softuni.bg/trainings/courses) Programme

## :heavy_check_mark: I. [Entry Module - February 2018](https://softuni.bg/modules/2/programming-basics)

### :heavy_check_mark: I.1. [Programming Basics With C# - February 2018](https://softuni.bg/trainings/1872/programming-basics-with-csharp-february-2018)
- - [ ] [01 - First Steps in Coding](https://judge.softuni.bg/Contests/150/First-Steps-in-Coding)
- - [ ] [02 - Simple Calculations](https://judge.softuni.bg/Contests/151/Simple-Calculations)
- - [ ] [03 - Simple Conditional Statements](https://judge.softuni.bg/Contests/152/Simple-Conditional-Statements)
- - [ ] [04 - Complex Conditional Statements](https://judge.softuni.bg/Contests/153/Complex-Conditional-Statements)
- - [ ] [05 - Simple Loops](https://judge.softuni.bg/Contests/154/Simple-Loops)
- - [ ] [06 - Drawing Figures with Loops](https://judge.softuni.bg/Contests/155/Drawing-Figures-with-Loops)
- - [ ] [07 - Advanced Loops](https://judge.softuni.bg/Contests/156/Advanced-Loops)

## II. [Tech Module - May 2018](https://softuni.bg/modules/19/tech-module)
### II.1. [Programming Fundamentals - May 2018](https://softuni.bg/trainings/1939/programming-fundamentals-may-2018)
- - [ ] 000 - Resources

- - [ ] 00 - Course Introduction

- - [ ] 01 - C# Intro and Basic Syntax
  - [ ] Lab
  - [ ] Exercises
  - [ ] More Exercises

- - [ ] 02 - C# Conditional Statements and Loops
  - [ ] Lab
  - [ ] Exercises

- - [ ] 03 - Data Types and Variables
  - [ ] Lab
  - [ ] Exercises
  - [ ] More Exercises

- - [ ] 04 - Methods. Debugging and Troubleshooting Code
  - [ ] Lab
  - [ ] Exercises
  - [ ] More Exercises

- - [ ] 05 - Arrays
  - [ ] Lab
  - [ ] Exercises
  - [ ] More Exercises

- - [ ] 06 - Lists
  - [ ] Lab
  - [ ] Exercises
  - [ ] More Exercises

- - [ ] 07 - Dictionaries, Lambda and LINQ
  - [ ] Lab
  - [ ] Exercises
  - [ ] More Exercises

- - [ ] 08 - Objects and Classes
  - [ ] Lab
  - [ ] Exercises
  - [ ] More Exercises

- - [ ] 09 - Strings and Text Processing
  - [ ] Lab
  - [ ] Exercises
  - [ ] More Exercises

- - [ ] 10 - Regular Expressions (RegEx)
  - [ ] Lab
  - [ ] Exercises
  - [ ] More Exercises

### II.2. [Software Technologies - June 2018](https://softuni.bg/trainings/1940/software-technologies-june-2018)
